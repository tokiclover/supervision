/*
 * Copyright (c) 2016-2019 tokiclover <tokiclover@gmail.com>
 * This file is part of Supervision
 *
 * Supervision is free software; you can redistribute
 * new BSD License included in the distriution of this package.
 *
 * @(#)timespec.c  0.15.0 2019/03/18
 */

#include "timespec.h"

uint64_t timespec_pack_sec(char *restrict b, register uint64_t u)
{
	register int o = TIMESPEC_NSIZ;
	while (o--) b[o] = (unsigned char)(u & 255), u >>= CHAR_BIT;
	b[0] = (unsigned char)u;
	return u;
}

__attribute__((__unused__)) uint64_t timespec_unpack_sec(char *restrict b)
{
	register uint64_t u;
	register int o = 0;
	u = (unsigned char)b[o];
	while (o++ < TIMESPEC_NSIZ) u <<= CHAR_BIT, u += (unsigned char)b[o];
	return u;
}

__attribute__((__unused__)) int timespec_cmp(struct timespec *restrict tu, struct timespec *restrict tv)
{
	if (tu->tv_sec  < tv->tv_sec ) return -1;
	if (tu->tv_sec  > tv->tv_sec ) return  1;
	if (tu->tv_nsec < tv->tv_nsec) return -1;
	if (tu->tv_nsec > tv->tv_nsec) return  1;
	return 0;
}

void timespec_pack(char *restrict b, struct timespec *restrict ta)
{
	register char *p = b;
	timespec_pack_sec(p, (uint64_t)ta->tv_sec  & UINT64_MAX);
	p += TIMESPEC_NSIZ;
	timespec_pack_sec(p, (uint64_t)ta->tv_nsec & TIMESPEC_NSEC_MAX);
	p += TIMESPEC_NSIZ;
}

void timespec_unpack(char *restrict b, struct timespec *restrict ta)
{
	register char *p = b;
	ta->tv_nsec = timespec_unpack_sec(p);
	p += TIMESPEC_NSIZ;
	ta->tv_sec  = timespec_unpack_sec(p);
}

void timespec_add(struct timespec *restrict ta, struct timespec *restrict tu, struct timespec *restrict tv)
{
	ta->tv_sec = tu->tv_sec + tv->tv_sec;

	if (TIMESPEC_NSEC_MAX <= tu->tv_nsec) {
		ta->tv_sec++;
		ta->tv_nsec = tv->tv_nsec;
	}
	else if (TIMESPEC_NSEC_MAX <= tv->tv_nsec) {
		ta->tv_sec++;
		ta->tv_nsec = tu->tv_nsec;
	}
	else {
		if ((tu->tv_nsec + tv->tv_nsec) > TIMESPEC_NSEC_MAX) {
			ta->tv_sec++;
			ta->tv_nsec = tu->tv_nsec > tv->tv_nsec ?
				tu->tv_nsec - TIMESPEC_NSEC_MAX - 1LU + tv->tv_nsec :
				tv->tv_nsec - TIMESPEC_NSEC_MAX - 1LU + tu->tv_nsec;
		}
		else
			ta->tv_nsec = tu->tv_nsec + tv->tv_nsec;
	}
}

void timespec_sub(struct timespec *restrict ta, struct timespec *restrict tu, struct timespec *restrict tv)
{
	ta->tv_sec = tu->tv_sec - tv->tv_sec;

	if (TIMESPEC_NSEC_MAX <= tu->tv_nsec) {
		ta->tv_nsec = tu->tv_nsec - tv->tv_nsec;
	}
	else if (TIMESPEC_NSEC_MAX <= tv->tv_nsec) {
		if (ta->tv_sec) ta->tv_sec--;
		ta->tv_nsec = tu->tv_nsec;
	}
	else {
		if (tu->tv_nsec > tv->tv_nsec)
			ta->tv_nsec = tu->tv_nsec - tv->tv_nsec;
		else {
			if (ta->tv_sec) ta->tv_sec--;
			ta->tv_nsec = TIMESPEC_NSEC_MAX + 1LU + (tu->tv_nsec - tv->tv_nsec);
		}
	}
}

