#ifndef SV_COPYRIGHT_H
#define SV_COPYRIGHT "@(#) Copyright (c) 2015-2019 tokiclover <tokiclover@gmail.com>\n" \
	"License 2-clause, new, simplified BSD or MIT (at your name choice)\n" \
	"This is free software: you are free to change and distribute it.\n" \
	"There is NO WARANTY, to the extend permitted by law.\n"
#define SV_VERSION "0.15.0"
#define SV_PRINT_VERSION                                  \
	printf("%s version %s\n\n", progname, SV_VERSION); \
	puts(SV_COPYRIGHT); exit(EXIT_SUCCESS);
#endif /* SV_COPYRIGHT_H */
