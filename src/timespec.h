/*
 * Copyright (c) 2016-2019 tokiclover <tokiclover@gmail.com>
 * This file is part of Supervision
 *
 * Supervision is free software; you can redistribute
 * new BSD License included in the distriution of this package.
 *
 * @(#)timespec.h  0.15.0 2019/03/14
 */

#ifndef TIMESPEC_H
#define TIMESPEC_H

#include <limits.h>
#include <sys/types.h>
#include <stdint.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

#define TIMESPEC_NSIZ 8LU
#define TIMESPEC_SIZE 16LU
#define TIMESPEC_NSEC_MAX 999999999LU
#define TIMESPEC_APPROX(ta) ((ta)->tv_sec+((ta)->tv_nsec > 50000000LU ? 1LU : OLU))

#define TIMESPEC(ta) clock_gettime(CLOCK_REALTIME, ta)

extern uint64_t timespec_pack_sec  (char *restrict b, register uint64_t u);
extern uint64_t timespec_unpack_sec(char *restrict b) __attribute__((__unused__));

extern void timespec_add(struct timespec *restrict ta, struct timespec *restrict tu, struct timespec *restrict tv);
extern void timespec_sub(struct timespec *restrict ta, struct timespec *restrict tu, struct timespec *restrict tv);
extern int  timespec_cmp(struct timespec *restrict tu, struct timespec *restrict tv) __attribute__((__unused__));

extern void timespec_pack  (char *restrict b, struct timespec *restrict ta);
extern void timespec_unpack(char *restrict b, struct timespec *restrict ta);

#ifdef __cplusplus
}
#endif
#endif /*TIMESPEC_H*/
